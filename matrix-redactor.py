#!/usr/bin/env python3

"""matrix-redactor
Redact Matrix room events for a user.
Usage examples:
./matrix-redactor.py --server 'https://matrix-client.matrix.org' --user '@user:matrix.org' --accesstoken 'AcCeSSTokenGibberisH'
    Show list of rooms.
./matrix-redactor.py --server 'https://matrix-client.matrix.org' --user '@user:matrix.org' --accesstoken 'AcCeSSTokenGibberisH' --room '!Abcdefghijklmnopqr:matrix.org'
    Redact all user's events in a room.
./matrix-redactor.py --server 'https://matrix-client.matrix.org' --user '@user:matrix.org' --accesstoken 'AcCeSSTokenGibberisH' --room '!Abcdefghijklmnopqr:matrix.org' --room '!Bcdefghijklmnopqrs:matrix.org'
    Redact all user's events in two rooms.
./matrix-redactor.py --server 'https://matrix-client.matrix.org' --user '@user:matrix.org' --accesstoken 'AcCeSSTokenGibberisH' --roomregex '.*:matrix.org'
    Redact all user's events in every room which matches a regex pattern.
./matrix-redactor.py --server 'https://matrix-client.matrix.org' --user '@user:matrix.org' --accesstoken 'AcCeSSTokenGibberisH' --all-rooms
    Redact all user's events in all available rooms.
"""

from nio import (
    AsyncClient,
    AsyncClientConfig,
    MatrixRoom,
    MessageDirection,
    RedactedEvent,
    RedactionEvent,
    Event,
    RoomRedactResponse,
    RoomRedactError,
    store,
    exceptions,
)
from functools import partial
import argparse
import asyncio
import re

CONFIG = {
    "homeserver": "",
    "user_id": "",
    "access_token": "",
    "rooms": [],
    "roomregex": "",
    "verbose": False,
}


def parse_args():
    """Parse arguments from command line"""

    parser = argparse.ArgumentParser(
        description=__doc__,
        add_help=False,  # Use individual setting below instead
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    parser.add_argument(
        "--help",
        action="help",
        help="""Show this help message and exit
             """,
    )
    parser.add_argument(
        "--server",
        metavar="HOST",
        required=True,
        help="""Set Matrix homeserver
             """,
    )
    parser.add_argument(
        "--user",
        metavar="USER_ID",
        required=True,
        help="""Set user ID
             """,
    )
    parser.add_argument(
        "--accesstoken",
        metavar="TOKEN",
        required=True,
        help="""Set access token
             """,
    )
    parser.add_argument(
        "--room",
        metavar="ROOM_ID",
        default=[],
        action="append",
        help="""Add room to list of rooms to redact
             """,
    )
    parser.add_argument(
        "--roomregex",
        metavar="PATTERN",
        default="",
        action="append",
        help="""Same as --room but by regex pattern
             """,
    )
    parser.add_argument(
        "--all-rooms",
        action="store_true",
        help="""Redact all rooms
             """,
    )
    parser.add_argument(
        "--verbose",
        action="store_true",
        help="""Print extra status messages
             """,
    )

    args = parser.parse_args()
    CONFIG["homeserver"] = args.server
    CONFIG["user_id"] = args.user
    CONFIG["access_token"] = args.accesstoken
    CONFIG["rooms"] = args.room
    CONFIG["roomregex"] = args.roomregex
    CONFIG["verbose"] = args.verbose
    if args.all_rooms:
        # Select all rooms by adding a regex pattern which matches any string
        CONFIG["roomregex"] = ".*"


async def create_client() -> AsyncClient:
    client = AsyncClient(
        homeserver=CONFIG["homeserver"],
        user=CONFIG["user_id"],
        config=AsyncClientConfig(store=store.SqliteMemoryStore),
    )
    client.access_token = CONFIG["access_token"]
    return client


async def show_rooms(client: AsyncClient):
    print("List of joined rooms (room id, display name):")
    for room_id, room in client.rooms.items():
        print(f"{room_id}, {room.display_name}")


def is_valid_event(event):
    events = (RedactedEvent, RedactionEvent)
    return isinstance(event, Event) and not isinstance(event, events)


async def fetch_room_events(
    client: AsyncClient,
    start_token: str,
    room: MatrixRoom,
    direction: MessageDirection,
) -> list:
    events = []
    while True:
        response = await client.room_messages(
            room.room_id,
            start_token,
            limit=1000,
            direction=direction,
            message_filter={
                "senders": [CONFIG["user_id"]],
            },
        )
        if len(response.chunk) == 0:
            break
        if CONFIG["verbose"]:
            response_length = len(response.chunk)
            print(f"Fetched {response_length} events.")
        events.extend(event for event in response.chunk if is_valid_event(event))
        start_token = response.end
    return events


async def redact_room_events(client, room):
    redacted_count = 0
    redact_error_count = 0
    print(f"Fetching and redacting {room.room_id} room events...")
    sync_resp = await client.sync(
        full_state=True, sync_filter={"room": {"timeline": {"limit": 1}}}
    )
    start_token = sync_resp.rooms.join[room.room_id].timeline.prev_batch
    # Generally, it should only be necessary to fetch back events but,
    # sometimes depending on the sync, front events need to be fetched
    # as well.
    fetch_room_events_ = partial(fetch_room_events, client, start_token, room)
    for events in [
        reversed(await fetch_room_events_(MessageDirection.back)),
        await fetch_room_events_(MessageDirection.front),
    ]:
        for event in events:
            try:
                if event.sender != CONFIG["user_id"]:
                    continue
                response = await client.room_redact(room.room_id, event.event_id)
                if isinstance(response, RoomRedactResponse):
                    if CONFIG["verbose"]:
                        print(f"Redacted event_id {event.event_id}.")
                    redacted_count += 1
                elif isinstance(response, RoomRedactError):
                    redact_error_count += 1
                    print(
                        f"Failed to redact event_id {event.event_id}: {response.message}."
                    )
                else:
                    print(
                        "Unknown room_redact response type. Shouldn't happen. Panicking!"
                    )
                    exit(1)
            except exceptions.EncryptionError as e:
                print(e, file=sys.stderr)
    print(
        f"Finished checking room events. Redacted {redacted_count} and failed to redact {redact_error_count} events."
    )


async def main() -> None:
    try:
        client = await create_client()
        print("Connecting...")
        await client.sync(
            full_state=True,
            # Limit fetch of room events as they will be fetched later
            sync_filter={"room": {"timeline": {"limit": 1}}},
        )
        if CONFIG["rooms"] == [] and CONFIG["roomregex"] == "":
            await show_rooms(client)
        else:
            for room_id, room in client.rooms.items():
                # Iterate over rooms to see if a room has been selected to
                # be automatically redacted
                if room_id in CONFIG["rooms"] or any(
                    re.match(pattern, room_id) for pattern in CONFIG["roomregex"]
                ):
                    print(f"Selected room: {room_id}")
                    await redact_room_events(client, room)
        print("Goodbye!")
        raise SystemExit
    except KeyboardInterrupt:
        sys.exit(1)
    finally:
        await client.close()


if __name__ == "__main__":
    parse_args()
    asyncio.get_event_loop().run_until_complete(main())
