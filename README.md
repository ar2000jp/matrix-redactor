# matrix-redactor

Redact Matrix room events for a user.

In other words, it will delete your Matrix messages for you.

# Installation

1. Python 3 required. Install it through your package manager.
2. Clone the repo and install dependencies
    ```
    git clone git@gitlab.com:ar2000jp/matrix-redactor.git
    cd matrix-redactor
    # Optional: create a virtualenv
    python -m venv venv && source venv/bin/activate
    pip install -r requirements.txt
    ```

# Usage

- You'll need to get your Matrix access token from another Matrix client
    - In Element, click on your profile name in the top left, go to 'All Settings', 'Help & About', 'Advanced' and copy the access token from there

- Run the app with ```--help``` for usage details
    ```
    ./matrix-redactor.py --help
    ```

- To see all rooms the user is in do
    ````
    ./matrix-redactor.py --server 'https://matrix-client.matrix.org' --user '@user:matrix.org' --accesstoken 'AcCeSSTokenGibberisH'
    ````
    Be sure to replace the parameters ('server', 'user' and 'accesstoken' in this case) with your own

- Copy the room ID you want to redact your events in from the output of the previous command and use it like this
    ````
    ./matrix-redactor.py --server 'https://matrix-client.matrix.org' --user '@user:matrix.org' --accesstoken 'AcCeSSTokenGibberisH' --room '!Abcdefghijklmnopqr:matrix.org'
    ````
- To redact all events in all the rooms the user is in do
    ````
    ./matrix-redactor.py --server 'https://matrix-client.matrix.org' --user '@user:matrix.org' --accesstoken 'AcCeSSTokenGibberisH' --all-rooms
    ````
